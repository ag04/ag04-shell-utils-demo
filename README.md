# SpringShell Utils Demo
Sample Command line interpreter (CLI) demo app developed with Spring Shell and ag04-shell-utils libraries.

![Banner](./assets/images/0-banner.png)

## Setup

```
git clone git@bitbucket.org:ag04/ag04-shell-utils-demo.git
```
To run it simply execute:

```
./grdlew clean build
java -jar build/libs/ag04-shell-utils-demo-0.0.1.jar
```

## Demo Commands
Projects consists of configuration and several demo commands, each demonstrating features of **ag04-spring-shell-utils** library.

### EchoCommand
This command demonstrates use of:
* Chalk
* ShellHelper
class in conveying contextual messages to user.

### TableCommand
Table command provides several examples of how to use SpringShell built in table capabilities.

### SignInCommand
This command demonstrates integration of SpringShell and Spring security.

### UserCommand
This command demonstrates use of:
* InputReader class to capture user input
* ProgressBar and ProgressCounter classes


Inspect code for more details on how to configure particlar component or util class.


## Contributors
- Domagoj Madunić


(c) AG04 Innovative Solutions d.o.o. (2019)

