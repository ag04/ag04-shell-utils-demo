package com.ag04.clidemo.command;

import com.ag04.utils.shell.Chalk;
import com.ag04.utils.shell.ShellHelper;
import com.ag04.utils.shell.progress.ProgressBar;
import com.ag04.utils.shell.progress.ProgressCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class EchoCommand {

    @Value("${shell.out.info:CYAN}")
    public String infoColor;

    @Value("${shell.out.success:GREEN}")
    public String successColor;

    @Value("${shell.out.warning:YELLOW}")
    public String warningColor;

    @Value("${shell.out.error:RED}")
    public String errorColor;

    @Autowired
    ShellHelper shellHelper;

    @Autowired
    ProgressBar progressBar;

    @Autowired
    ProgressCounter progressCounter;


    @ShellMethod("Displays greeting message to the user whose name is supplied")
    public String echo(@ShellOption({"-N", "--name"}) String name) {
        String message = String.format("Hello %s!", name);
        shellHelper.println(message.concat(" (Default style message)"));
        shellHelper.printlnError(message.concat(" (Error style message)"));
        shellHelper.printlnWarning(message.concat(" (Warning style message)"));
        shellHelper.printlnInfo(message.concat(" (Info style message)"));
        shellHelper.printlnSuccess(message.concat(" (Success style message)"));

        String output = Chalk.color(message, successColor);
        return output.concat(" You are running spring shell cli-demo.");
    }

    @ShellMethod("Displays progress spinner")
    public void progressSpinner() throws InterruptedException {
        for (int i = 1; i <=100; i++) {
            progressCounter.display();
            Thread.sleep(100);
        }
        progressCounter.reset();
    }

    @ShellMethod("Displays progress counter (with spinner)")
    public void progressCounter() throws InterruptedException {
        for (int i = 1; i <=100; i++) {
            progressCounter.display(i, "Processing");
            Thread.sleep(100);
        }
        progressCounter.reset();
    }

    @ShellMethod("Displays progress bar")
    public void progressBar() throws InterruptedException {
        for (int i = 1; i <=100; i++) {
            progressBar.display(i);
            Thread.sleep(100);
        }
        progressBar.reset();
    }

}
