package com.ag04.clidemo.observer;

import com.ag04.utils.shell.Chalk;
import com.ag04.utils.shell.ShellHelper;
import com.ag04.utils.shell.progress.ProgressBar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Observable;
import java.util.Observer;

@Service
public class ProgressUpdateObserver implements Observer {

    @Value("${shell.out.warning:YELLOW}")
    public String warningColor;

    @Autowired
    private ProgressBar progressBar;

    @Autowired
    private ShellHelper shellHelper;

    @Override
    public void update(Observable observable, Object event) {
        ProgressUpdateEvent upe = (ProgressUpdateEvent) event;
        int currentRecord = upe.getCurrentCount().intValue();
        int totalRecords = upe.getTotalCount().intValue();

        if (currentRecord == 0) {
            // just in case the previous progress bar was interrupted
            progressBar.reset();
        }

        String message = null;
        int percentage = currentRecord * 100 / totalRecords;
        if (StringUtils.hasText(upe.getMessage())) {
            message = Chalk.color(upe.getMessage(), warningColor);
            progressBar.display(percentage, message);
        }

        progressBar.display(percentage, message);
        if (percentage == 100) {
            progressBar.reset();
        }
    }
}
